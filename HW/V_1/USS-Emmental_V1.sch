EESchema Schematic File Version 5
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
Comment5 ""
Comment6 ""
Comment7 ""
Comment8 ""
Comment9 ""
$EndDescr
$Comp
L PC104_board-rescue:HOLE-MLAB_MECHANICAL M1
U 1 1 5D096865
P 8650 1825
F 0 "M1" H 8669 2039 60  0000 C CNN
F 1 "HOLE" H 8669 1933 60  0000 C CNN
F 2 "Mounting_Holes:MountingHole_3.7mm_Pad" H 8669 1933 60  0001 C CNN
F 3 "" H 8650 1825 60  0000 C CNN
	1    8650 1825
	1    0    0    -1  
$EndComp
$Comp
L PC104_board-rescue:HOLE-MLAB_MECHANICAL M2
U 1 1 5D096AD3
P 8700 2350
F 0 "M2" H 8719 2564 60  0000 C CNN
F 1 "HOLE" H 8719 2458 60  0000 C CNN
F 2 "Mounting_Holes:MountingHole_3.7mm_Pad" H 8719 2458 60  0001 C CNN
F 3 "" H 8700 2350 60  0000 C CNN
	1    8700 2350
	1    0    0    -1  
$EndComp
$Comp
L PC104_board-rescue:HOLE-MLAB_MECHANICAL M3
U 1 1 5D096D66
P 8725 2625
F 0 "M3" H 8744 2839 60  0000 C CNN
F 1 "HOLE" H 8744 2733 60  0000 C CNN
F 2 "Mounting_Holes:MountingHole_3.7mm_Pad" H 8744 2733 60  0001 C CNN
F 3 "" H 8725 2625 60  0000 C CNN
	1    8725 2625
	1    0    0    -1  
$EndComp
$Comp
L PC104_board-rescue:HOLE-MLAB_MECHANICAL M4
U 1 1 5D097003
P 8700 2875
F 0 "M4" H 8719 3089 60  0000 C CNN
F 1 "HOLE" H 8719 2983 60  0000 C CNN
F 2 "Mounting_Holes:MountingHole_3.7mm_Pad" H 8719 2983 60  0001 C CNN
F 3 "" H 8700 2875 60  0000 C CNN
	1    8700 2875
	1    0    0    -1  
$EndComp
$Comp
L PC104_board-rescue:HOLE-MLAB_MECHANICAL M7
U 1 1 5D767C7E
P 8750 4250
F 0 "M7" H 8769 4464 60  0000 C CNN
F 1 "HOLE" H 8769 4358 60  0000 C CNN
F 2 "Mlab_Mechanical:MountingHole_3mm" H 8769 4358 60  0001 C CNN
F 3 "" H 8750 4250 60  0000 C CNN
	1    8750 4250
	1    0    0    -1  
$EndComp
$Comp
L PC104_board-rescue:HOLE-MLAB_MECHANICAL M5
U 1 1 5D767C8C
P 8700 3725
F 0 "M5" H 8719 3939 60  0000 C CNN
F 1 "HOLE" H 8719 3833 60  0000 C CNN
F 2 "Mlab_Mechanical:MountingHole_3mm" H 8719 3833 60  0001 C CNN
F 3 "" H 8700 3725 60  0000 C CNN
	1    8700 3725
	1    0    0    -1  
$EndComp
$Comp
L PC104_board-rescue:HOLE-MLAB_MECHANICAL M9
U 1 1 5D767C9A
P 8775 4525
F 0 "M9" H 8794 4739 60  0000 C CNN
F 1 "HOLE" H 8794 4633 60  0000 C CNN
F 2 "Mlab_Mechanical:MountingHole_3mm" H 8794 4633 60  0001 C CNN
F 3 "" H 8775 4525 60  0000 C CNN
	1    8775 4525
	1    0    0    -1  
$EndComp
$Comp
L PC104_board-rescue:HOLE-MLAB_MECHANICAL M11
U 1 1 5D767CA8
P 8750 4775
F 0 "M11" H 8769 4989 60  0000 C CNN
F 1 "HOLE" H 8769 4883 60  0000 C CNN
F 2 "Mlab_Mechanical:MountingHole_3mm" H 8769 4883 60  0001 C CNN
F 3 "" H 8750 4775 60  0000 C CNN
	1    8750 4775
	1    0    0    -1  
$EndComp
$Comp
L PC104_board-rescue:HOLE-MLAB_MECHANICAL M8
U 1 1 5D7686B6
P 9750 4275
F 0 "M8" H 9769 4489 60  0000 C CNN
F 1 "HOLE" H 9769 4383 60  0000 C CNN
F 2 "Mlab_Mechanical:MountingHole_3mm" H 9769 4383 60  0001 C CNN
F 3 "" H 9750 4275 60  0000 C CNN
	1    9750 4275
	1    0    0    -1  
$EndComp
$Comp
L PC104_board-rescue:HOLE-MLAB_MECHANICAL M6
U 1 1 5D7686C4
P 9700 3750
F 0 "M6" H 9719 3964 60  0000 C CNN
F 1 "HOLE" H 9719 3858 60  0000 C CNN
F 2 "Mlab_Mechanical:MountingHole_3mm" H 9719 3858 60  0001 C CNN
F 3 "" H 9700 3750 60  0000 C CNN
	1    9700 3750
	1    0    0    -1  
$EndComp
$Comp
L PC104_board-rescue:HOLE-MLAB_MECHANICAL M10
U 1 1 5D7686D2
P 9775 4550
F 0 "M10" H 9794 4764 60  0000 C CNN
F 1 "HOLE" H 9794 4658 60  0000 C CNN
F 2 "Mlab_Mechanical:MountingHole_3mm" H 9794 4658 60  0001 C CNN
F 3 "" H 9775 4550 60  0000 C CNN
	1    9775 4550
	1    0    0    -1  
$EndComp
$Comp
L PC104_board-rescue:HOLE-MLAB_MECHANICAL M12
U 1 1 5D7686E0
P 9750 4800
F 0 "M12" H 9769 5014 60  0000 C CNN
F 1 "HOLE" H 9769 4908 60  0000 C CNN
F 2 "Mlab_Mechanical:MountingHole_3mm" H 9769 4908 60  0001 C CNN
F 3 "" H 9750 4800 60  0000 C CNN
	1    9750 4800
	1    0    0    -1  
$EndComp
Wire Wire Line
	7550 3850 7600 3850
Wire Wire Line
	7650 3950 7600 3950
Wire Wire Line
	7550 4050 7600 4050
Wire Wire Line
	7650 4150 7600 4150
Wire Wire Line
	7550 4250 7600 4250
Wire Wire Line
	7650 4350 7600 4350
Wire Wire Line
	7550 4450 7600 4450
Wire Wire Line
	7650 4550 7600 4550
Wire Wire Line
	7550 4650 7600 4650
Connection ~ 6450 2250
Wire Wire Line
	6450 2250 6450 2350
Connection ~ 6450 2150
Wire Wire Line
	6450 2150 6450 2250
Wire Wire Line
	6450 2050 6450 2150
Connection ~ 6450 2050
Connection ~ 6450 1950
Wire Wire Line
	6450 1950 6450 2050
Connection ~ 6450 1850
Wire Wire Line
	6450 1850 6450 1950
Connection ~ 6450 1750
Wire Wire Line
	6450 1750 6450 1850
Connection ~ 6450 1650
Wire Wire Line
	6450 1650 6450 1750
Wire Wire Line
	7600 2250 7600 2150
Wire Wire Line
	7650 1750 7600 1750
Connection ~ 7600 1750
Wire Wire Line
	7600 1750 7550 1750
Wire Wire Line
	7600 1750 7600 1850
Wire Wire Line
	7550 3050 7600 3050
Wire Wire Line
	7650 3150 7600 3150
Wire Wire Line
	7550 1650 7600 1650
Wire Wire Line
	7650 1550 7600 1550
Wire Wire Line
	7600 1450 7600 1550
Wire Wire Line
	7600 1550 7550 1550
Wire Wire Line
	7600 1550 7600 1650
Wire Wire Line
	7600 1650 7650 1650
Wire Wire Line
	7550 1450 7600 1450
Wire Wire Line
	7650 1350 7600 1350
Wire Wire Line
	7600 1350 7600 1450
Wire Wire Line
	7600 1350 7550 1350
Wire Wire Line
	7600 1450 7650 1450
Connection ~ 7600 1350
Connection ~ 7600 1450
Connection ~ 7600 1550
Connection ~ 7600 1650
Wire Wire Line
	7600 1750 7600 1650
Wire Wire Line
	7550 2450 7600 2450
Wire Wire Line
	7600 2450 7600 2550
Connection ~ 7600 2450
Wire Wire Line
	7600 2450 7650 2450
Wire Wire Line
	7550 2350 7600 2350
Wire Wire Line
	7600 2350 7650 2350
Wire Wire Line
	7600 2350 7600 2450
Wire Wire Line
	7550 2150 7600 2150
Wire Wire Line
	7600 2050 7600 2150
Connection ~ 7600 2150
Wire Wire Line
	7600 2150 7650 2150
Wire Wire Line
	7550 1950 7600 1950
Wire Wire Line
	7650 1850 7600 1850
Wire Wire Line
	7600 1850 7600 1950
Connection ~ 7600 1850
Wire Wire Line
	7600 1850 7550 1850
Wire Wire Line
	7600 1950 7650 1950
Wire Wire Line
	7650 2050 7600 2050
Connection ~ 7600 1950
Wire Wire Line
	7600 1950 7600 2050
Connection ~ 7600 2050
Wire Wire Line
	7600 2050 7550 2050
Wire Wire Line
	7650 2250 7600 2250
Connection ~ 7600 2250
Wire Wire Line
	7600 2250 7550 2250
Wire Wire Line
	7600 2250 7600 2350
Connection ~ 7600 2350
Wire Wire Line
	6450 4650 6500 4650
Wire Wire Line
	6500 4550 6450 4550
Wire Wire Line
	6500 4450 6450 4450
Wire Wire Line
	6500 4350 6450 4350
Wire Wire Line
	6500 4250 6450 4250
Wire Wire Line
	6500 4150 6450 4150
Wire Wire Line
	6500 4050 6450 4050
Wire Wire Line
	6500 3950 6450 3950
Wire Wire Line
	6500 3850 6450 3850
Wire Wire Line
	6500 3750 6450 3750
Wire Wire Line
	6500 2650 6450 2650
Wire Wire Line
	6500 2550 6450 2550
Wire Wire Line
	6500 2450 6450 2450
Wire Wire Line
	6500 2350 6450 2350
Wire Wire Line
	6500 2250 6450 2250
Wire Wire Line
	6500 2150 6450 2150
Wire Wire Line
	6500 2050 6450 2050
Wire Wire Line
	6500 1950 6450 1950
Wire Wire Line
	6500 1850 6450 1850
Wire Wire Line
	6500 1750 6450 1750
Wire Wire Line
	6500 1650 6450 1650
Wire Wire Line
	7650 3350 7600 3350
Wire Wire Line
	7550 3450 7600 3450
Wire Wire Line
	7650 3550 7600 3550
Wire Wire Line
	7550 3650 7600 3650
Wire Wire Line
	7550 3750 7600 3750
$Comp
L Connector_Generic:Conn_01x32 J8
U 1 1 5DA24661
P 7850 2950
F 0 "J8" H 7768 4767 50  0000 C CNN
F 1 "Conn_01x34" H 7768 4676 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x34_P2.54mm_Vertical" H 7850 2950 50  0001 C CNN
F 3 "~" H 7850 2950 50  0001 C CNN
	1    7850 2950
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x32 J7
U 1 1 5DA34341
P 7350 2950
F 0 "J7" H 7268 4767 50  0000 C CNN
F 1 "Conn_01x34" H 7268 4676 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x34_P2.54mm_Vertical" H 7350 2950 50  0001 C CNN
F 3 "~" H 7350 2950 50  0001 C CNN
	1    7350 2950
	-1   0    0    -1  
$EndComp
Wire Wire Line
	2500 2000 2350 2000
Wire Wire Line
	2500 1900 2350 1900
Wire Wire Line
	2350 2600 2500 2600
Wire Wire Line
	2350 2700 2500 2700
Wire Wire Line
	2500 2300 2350 2300
Wire Wire Line
	2500 2400 2350 2400
Wire Wire Line
	2500 2100 2350 2100
Text GLabel 2350 2300 0    50   Input ~ 0
2.21
Text GLabel 2350 2100 0    50   Input ~ 0
2.17
Wire Wire Line
	3000 2600 3150 2600
Wire Wire Line
	3000 2200 3150 2200
Wire Wire Line
	3000 2300 3150 2300
Wire Wire Line
	3000 2500 3150 2500
Wire Wire Line
	3000 2400 3150 2400
Wire Wire Line
	3000 2000 3150 2000
Wire Wire Line
	3000 2100 3150 2100
Wire Wire Line
	3000 1900 3150 1900
Wire Wire Line
	3000 2700 3150 2700
Wire Wire Line
	2500 2500 2350 2500
Text GLabel 2350 2800 0    50   Input ~ 0
2.31
Text GLabel 2350 2600 0    50   Input ~ 0
2.27
Text GLabel 2350 2500 0    50   Input ~ 0
2.25
Text GLabel 2350 2700 0    50   Input ~ 0
2.29
Wire Wire Line
	2500 5350 2350 5350
Wire Wire Line
	3000 5350 3150 5350
Wire Wire Line
	3000 5450 3150 5450
Wire Wire Line
	3000 5550 3150 5550
Wire Wire Line
	3000 5150 3150 5150
Wire Wire Line
	3000 4850 3150 4850
Wire Wire Line
	3000 4950 3150 4950
Wire Wire Line
	3000 5050 3150 5050
Wire Wire Line
	2500 5750 2350 5750
Wire Wire Line
	2500 5950 2350 5950
Wire Wire Line
	2500 5450 2350 5450
Wire Wire Line
	2500 5850 2350 5850
Wire Wire Line
	2500 5550 2350 5550
Wire Wire Line
	2500 5150 2350 5150
Wire Wire Line
	2500 5650 2350 5650
Wire Wire Line
	3000 5850 3150 5850
Wire Wire Line
	3150 5650 3000 5650
Wire Wire Line
	3000 5750 3150 5750
Text GLabel 3150 5550 2    50   Input ~ 0
2.28
Text GLabel 3150 5350 2    50   Input ~ 0
2.24
Text GLabel 3150 5850 2    50   Input ~ 0
2.34
Text GLabel 3150 5450 2    50   Input ~ 0
2.26
Text GLabel 3150 5650 2    50   Input ~ 0
2.30
Wire Wire Line
	3000 4750 3150 4750
Wire Wire Line
	3000 4650 3150 4650
Wire Wire Line
	3000 4450 3150 4450
Wire Wire Line
	3000 4550 3150 4550
Text GLabel 3150 4650 2    50   Input ~ 0
2.10
Text GLabel 3150 4550 2    50   Input ~ 0
2.8
Text GLabel 3150 4750 2    50   Input ~ 0
2.12
Text GLabel 3150 4950 2    50   Input ~ 0
2.16
Text GLabel 3150 5050 2    50   Input ~ 0
2.18
Text GLabel 3150 5250 2    50   Input ~ 0
2.22
Text GLabel 3150 4850 2    50   Input ~ 0
2.14
Text GLabel 3150 5150 2    50   Input ~ 0
2.20
$Comp
L PC104_board-rescue:HEADER_2x26-MLAB_HEADER J2
U 1 1 5D9B2C4B
P 2750 5500
F 0 "J2" H 2750 7103 60  0000 C CNN
F 1 "HEADER_2x26" H 2750 6997 60  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x26_P2.54mm_Vertical" H 2750 6891 60  0001 C CNN
F 3 "" H 2750 6750 60  0000 C CNN
	1    2750 5500
	1    0    0    -1  
$EndComp
Wire Wire Line
	3000 4350 3150 4350
Wire Wire Line
	3000 4250 3150 4250
Wire Wire Line
	3000 5250 3150 5250
Text GLabel 3150 4250 2    50   Input ~ 0
2.2
Text GLabel 2350 5650 0    50   Input ~ 0
2.29
Text GLabel 3150 4450 2    50   Input ~ 0
2.6
Text GLabel 3150 4350 2    50   Input ~ 0
2.4
Wire Wire Line
	3000 3800 3150 3800
Wire Wire Line
	3000 3700 3150 3700
Wire Wire Line
	3000 3600 3150 3600
Wire Wire Line
	2350 3800 2500 3800
Wire Wire Line
	2500 4850 2350 4850
Wire Wire Line
	2500 4950 2350 4950
Wire Wire Line
	2500 5250 2350 5250
Wire Wire Line
	2500 5050 2350 5050
Text GLabel 2350 4450 0    50   Input ~ 0
2.5
Text GLabel 2350 4250 0    50   Input ~ 0
2.1
Text GLabel 2350 4350 0    50   Input ~ 0
2.3
Text GLabel 2350 4950 0    50   Input ~ 0
2.15
Text GLabel 2350 4850 0    50   Input ~ 0
2.13
Text GLabel 2350 4750 0    50   Input ~ 0
2.11
Text GLabel 2350 4650 0    50   Input ~ 0
2.9
Text GLabel 2350 4550 0    50   Input ~ 0
2.7
Text GLabel 2350 5150 0    50   Input ~ 0
2.19
Text GLabel 2350 5250 0    50   Input ~ 0
2.21
Text GLabel 2350 5050 0    50   Input ~ 0
2.17
Text GLabel 2350 5550 0    50   Input ~ 0
2.27
Text GLabel 2350 5450 0    50   Input ~ 0
2.25
Text GLabel 2350 5350 0    50   Input ~ 0
2.23
Wire Wire Line
	2350 1600 2500 1600
Wire Wire Line
	2500 1400 2350 1400
Wire Wire Line
	2500 1500 2350 1500
Wire Wire Line
	2500 1300 2350 1300
Wire Wire Line
	2500 1800 2350 1800
Wire Wire Line
	2500 1700 2350 1700
Wire Wire Line
	3000 1700 3150 1700
Wire Wire Line
	3000 1600 3150 1600
Wire Wire Line
	3000 1800 3150 1800
Wire Wire Line
	3000 1500 3150 1500
Wire Wire Line
	3000 1300 3150 1300
Wire Wire Line
	3000 1400 3150 1400
Text GLabel 3150 1300 2    50   Input ~ 0
2.2
Text GLabel 3150 1400 2    50   Input ~ 0
2.4
Text GLabel 2350 1500 0    50   Input ~ 0
2.5
Text GLabel 2350 1600 0    50   Input ~ 0
2.7
Text GLabel 2350 1400 0    50   Input ~ 0
2.3
Text GLabel 2350 1700 0    50   Input ~ 0
2.9
Text GLabel 2350 1800 0    50   Input ~ 0
2.11
$Comp
L PC104_board-rescue:HEADER_2x26-MLAB_HEADER J2
U 1 1 5D9B2D0C
P 2750 2550
F 0 "J2" H 2750 4153 60  0000 C CNN
F 1 "HEADER_2x26" H 2750 4047 60  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x26_P2.54mm_Vertical" H 2750 3941 60  0001 C CNN
F 3 "" H 2750 3800 60  0000 C CNN
	1    2750 2550
	1    0    0    -1  
$EndComp
Text GLabel 3150 1900 2    50   Input ~ 0
2.14
Text GLabel 3150 2000 2    50   Input ~ 0
2.16
Text GLabel 3150 2100 2    50   Input ~ 0
2.18
Text GLabel 3150 2700 2    50   Input ~ 0
2.30
Text GLabel 3150 2500 2    50   Input ~ 0
2.26
Text GLabel 3150 2400 2    50   Input ~ 0
2.24
Text GLabel 3150 2200 2    50   Input ~ 0
2.20
Text GLabel 3150 2600 2    50   Input ~ 0
2.28
Text GLabel 3150 2300 2    50   Input ~ 0
2.22
Text GLabel 2350 2400 0    50   Input ~ 0
2.23
Text GLabel 2350 2200 0    50   Input ~ 0
2.19
Text GLabel 2350 2000 0    50   Input ~ 0
2.15
Text GLabel 2350 1900 0    50   Input ~ 0
2.13
Text GLabel 2350 1300 0    50   Input ~ 0
2.1
Text GLabel 3150 1800 2    50   Input ~ 0
2.12
Text GLabel 3150 1700 2    50   Input ~ 0
2.10
Text GLabel 3150 1500 2    50   Input ~ 0
2.6
Text GLabel 3150 1600 2    50   Input ~ 0
2.8
Wire Wire Line
	2500 6150 2350 6150
Wire Wire Line
	2500 6250 2350 6250
Wire Wire Line
	2500 6050 2350 6050
Text GLabel 2350 6150 0    50   Input ~ 0
2.39
Text GLabel 2350 6250 0    50   Input ~ 0
2.41
Wire Wire Line
	2500 6650 2350 6650
Wire Wire Line
	3000 6750 3150 6750
Wire Wire Line
	3000 6650 3150 6650
Wire Wire Line
	2500 6750 2350 6750
Wire Wire Line
	3000 5950 3150 5950
Wire Wire Line
	3000 6050 3150 6050
Wire Wire Line
	3000 6150 3150 6150
Text GLabel 3150 6150 2    50   Input ~ 0
2.40
Text GLabel 3150 5950 2    50   Input ~ 0
2.36
Text GLabel 3150 6050 2    50   Input ~ 0
2.38
Text GLabel 3150 5750 2    50   Input ~ 0
2.32
Wire Wire Line
	3000 6250 3150 6250
Wire Wire Line
	3000 6350 3150 6350
Wire Wire Line
	3000 6450 3150 6450
Text GLabel 3150 6450 2    50   Input ~ 0
2.46
Text GLabel 3150 6250 2    50   Input ~ 0
2.42
Text GLabel 3150 6350 2    50   Input ~ 0
2.44
Wire Wire Line
	2500 6350 2350 6350
Wire Wire Line
	2500 6550 2350 6550
Wire Wire Line
	2500 6450 2350 6450
Text GLabel 2350 6750 0    50   Input ~ 0
2.51
Text GLabel 2350 6650 0    50   Input ~ 0
2.49
Text GLabel 2350 6550 0    50   Input ~ 0
2.47
Text GLabel 2350 6350 0    50   Input ~ 0
2.43
Text GLabel 2350 6450 0    50   Input ~ 0
2.45
Wire Wire Line
	3000 6550 3150 6550
Text GLabel 3150 6550 2    50   Input ~ 0
2.48
Text GLabel 3150 6750 2    50   Input ~ 0
2.52
Text GLabel 3150 6650 2    50   Input ~ 0
2.50
Text GLabel 2350 6050 0    50   Input ~ 0
2.37
Text GLabel 2350 5750 0    50   Input ~ 0
2.31
Text GLabel 2350 5950 0    50   Input ~ 0
2.35
Text GLabel 2350 5850 0    50   Input ~ 0
2.33
Wire Wire Line
	2350 2800 2500 2800
Wire Wire Line
	2350 2900 2500 2900
Wire Wire Line
	2350 3000 2500 3000
Wire Wire Line
	2350 3100 2500 3100
Wire Wire Line
	3000 2800 3150 2800
Wire Wire Line
	3000 3100 3150 3100
Wire Wire Line
	3000 2900 3150 2900
Wire Wire Line
	3000 3000 3150 3000
Wire Wire Line
	2350 3400 2500 3400
Wire Wire Line
	2350 3500 2500 3500
Wire Wire Line
	2350 3600 2500 3600
Wire Wire Line
	2350 3700 2500 3700
Wire Wire Line
	2350 3300 2500 3300
Text GLabel 2350 3000 0    50   Input ~ 0
2.35
Text GLabel 2350 2900 0    50   Input ~ 0
2.33
Text GLabel 2350 3300 0    50   Input ~ 0
2.41
Text GLabel 2350 3200 0    50   Input ~ 0
2.39
Text GLabel 2350 3100 0    50   Input ~ 0
2.37
Wire Wire Line
	3000 3500 3150 3500
Text GLabel 3150 3800 2    50   Input ~ 0
2.52
Text GLabel 3150 3700 2    50   Input ~ 0
2.50
Text GLabel 3150 3400 2    50   Input ~ 0
2.44
Text GLabel 3150 3600 2    50   Input ~ 0
2.48
Text GLabel 3150 3300 2    50   Input ~ 0
2.42
Text GLabel 3150 3000 2    50   Input ~ 0
2.36
Text GLabel 2350 3600 0    50   Input ~ 0
2.47
Text GLabel 2350 3400 0    50   Input ~ 0
2.43
Text GLabel 2350 3800 0    50   Input ~ 0
2.51
Text GLabel 2350 3500 0    50   Input ~ 0
2.45
Text GLabel 2350 3700 0    50   Input ~ 0
2.49
Wire Wire Line
	5250 3700 5400 3700
Wire Wire Line
	5250 3300 5400 3300
Wire Wire Line
	5250 3600 5400 3600
Wire Wire Line
	5250 3500 5400 3500
Wire Wire Line
	5250 3400 5400 3400
Wire Wire Line
	5250 4450 5400 4450
Wire Wire Line
	5250 4250 5400 4250
Wire Wire Line
	2350 4450 2500 4450
Wire Wire Line
	2500 4550 2350 4550
Wire Wire Line
	2500 4750 2350 4750
Wire Wire Line
	2500 4650 2350 4650
Wire Wire Line
	2500 4350 2350 4350
Wire Wire Line
	2500 2200 2350 2200
Wire Wire Line
	2350 3200 2500 3200
Wire Wire Line
	3000 3200 3150 3200
Text GLabel 3150 2800 2    50   Input ~ 0
2.32
Text GLabel 3150 2900 2    50   Input ~ 0
2.34
Wire Wire Line
	2500 4250 2350 4250
Wire Wire Line
	3000 3400 3150 3400
Wire Wire Line
	3000 3300 3150 3300
Text GLabel 3150 3500 2    50   Input ~ 0
2.46
Wire Wire Line
	4600 3600 4750 3600
Wire Wire Line
	4600 3500 4750 3500
Wire Wire Line
	4600 3700 4750 3700
Wire Wire Line
	4600 3300 4750 3300
Wire Wire Line
	4600 3400 4750 3400
Wire Wire Line
	4600 2500 4750 2500
Text GLabel 3150 3200 2    50   Input ~ 0
2.40
Text GLabel 3150 3100 2    50   Input ~ 0
2.38
Text GLabel 4600 3200 0    50   Input ~ 0
1.39
Wire Wire Line
	4600 4650 4750 4650
Wire Wire Line
	4600 4550 4750 4550
Text GLabel 4600 4250 0    50   Input ~ 0
1.1
Text GLabel 4600 4350 0    50   Input ~ 0
1.3
Text GLabel 4600 4650 0    50   Input ~ 0
1.9
Text GLabel 4600 3300 0    50   Input ~ 0
1.41
Text GLabel 4600 3700 0    50   Input ~ 0
1.49
Text GLabel 4600 3500 0    50   Input ~ 0
1.45
Text GLabel 4600 3600 0    50   Input ~ 0
1.47
Text GLabel 4600 3400 0    50   Input ~ 0
1.43
Text GLabel 4600 3800 0    50   Input ~ 0
1.51
Wire Wire Line
	4600 1300 4750 1300
Wire Wire Line
	4600 1600 4750 1600
Wire Wire Line
	4600 1700 4750 1700
Wire Wire Line
	4600 1400 4750 1400
Wire Wire Line
	4600 1500 4750 1500
Wire Wire Line
	5250 1400 5400 1400
Wire Wire Line
	5250 1300 5400 1300
Text GLabel 4600 1300 0    50   Input ~ 0
1.1
Text GLabel 4600 1600 0    50   Input ~ 0
1.7
Text GLabel 4600 1400 0    50   Input ~ 0
1.3
Text GLabel 4600 1500 0    50   Input ~ 0
1.5
Wire Wire Line
	5250 1700 5400 1700
Wire Wire Line
	5250 1500 5400 1500
Wire Wire Line
	5250 1900 5400 1900
Text GLabel 5400 1300 2    50   Input ~ 0
1.2
Text GLabel 5400 1600 2    50   Input ~ 0
1.8
Text GLabel 5400 1500 2    50   Input ~ 0
1.6
Text GLabel 5400 1400 2    50   Input ~ 0
1.4
Text GLabel 5400 1800 2    50   Input ~ 0
1.12
Text GLabel 5400 2000 2    50   Input ~ 0
1.16
Text GLabel 5400 2200 2    50   Input ~ 0
1.20
Text GLabel 5400 2300 2    50   Input ~ 0
1.22
Text GLabel 5400 2100 2    50   Input ~ 0
1.18
Text GLabel 5400 1900 2    50   Input ~ 0
1.14
Wire Wire Line
	4600 6050 4750 6050
Wire Wire Line
	4600 6350 4750 6350
Wire Wire Line
	4600 6250 4750 6250
Wire Wire Line
	4600 6150 4750 6150
Text GLabel 4600 6150 0    50   Input ~ 0
1.39
Text GLabel 4600 6250 0    50   Input ~ 0
1.41
Text GLabel 4600 6350 0    50   Input ~ 0
1.43
Wire Wire Line
	5250 6550 5400 6550
Wire Wire Line
	5250 6450 5400 6450
Wire Wire Line
	5250 6350 5400 6350
Text GLabel 5400 6350 2    50   Input ~ 0
1.44
Text GLabel 5400 6450 2    50   Input ~ 0
1.46
Wire Wire Line
	5250 5950 5400 5950
Wire Wire Line
	5250 5750 5400 5750
Wire Wire Line
	5250 5850 5400 5850
Text GLabel 5400 5850 2    50   Input ~ 0
1.34
Text GLabel 5400 5950 2    50   Input ~ 0
1.36
Text GLabel 5400 5750 2    50   Input ~ 0
1.32
Wire Wire Line
	4600 5650 4750 5650
Wire Wire Line
	4600 5750 4750 5750
Wire Wire Line
	4600 5850 4750 5850
Wire Wire Line
	4600 5950 4750 5950
Text GLabel 4600 5850 0    50   Input ~ 0
1.33
Text GLabel 4600 6050 0    50   Input ~ 0
1.37
Text GLabel 4600 5950 0    50   Input ~ 0
1.35
Text GLabel 4600 5750 0    50   Input ~ 0
1.31
Wire Wire Line
	4600 6650 4750 6650
Wire Wire Line
	4600 6750 4750 6750
Wire Wire Line
	4600 6450 4750 6450
Wire Wire Line
	4600 6550 4750 6550
Text GLabel 4600 6650 0    50   Input ~ 0
1.49
Text GLabel 4600 6750 0    50   Input ~ 0
1.51
Text GLabel 4600 6450 0    50   Input ~ 0
1.45
Text GLabel 4600 6550 0    50   Input ~ 0
1.47
Wire Wire Line
	5250 6750 5400 6750
Wire Wire Line
	5250 6650 5400 6650
Text GLabel 5400 6750 2    50   Input ~ 0
1.52
Text GLabel 5400 6550 2    50   Input ~ 0
1.48
Text GLabel 5400 6650 2    50   Input ~ 0
1.50
Wire Wire Line
	5250 6250 5400 6250
Wire Wire Line
	5250 6050 5400 6050
Wire Wire Line
	5250 6150 5400 6150
Text GLabel 5400 6050 2    50   Input ~ 0
1.38
Text GLabel 5400 6150 2    50   Input ~ 0
1.40
Text GLabel 5400 6250 2    50   Input ~ 0
1.42
Wire Wire Line
	4600 3800 4750 3800
Wire Wire Line
	5250 3800 5400 3800
Wire Wire Line
	4600 4450 4750 4450
Wire Wire Line
	4600 4750 4750 4750
Wire Wire Line
	5250 4550 5400 4550
Wire Wire Line
	5250 4350 5400 4350
Wire Wire Line
	4600 4250 4750 4250
Wire Wire Line
	4600 4350 4750 4350
Wire Wire Line
	5250 4650 5400 4650
Wire Wire Line
	4600 4850 4750 4850
Wire Wire Line
	4600 4950 4750 4950
Wire Wire Line
	4600 5050 4750 5050
Wire Wire Line
	4600 5150 4750 5150
Text GLabel 4600 4450 0    50   Input ~ 0
1.5
Text GLabel 4600 4550 0    50   Input ~ 0
1.7
Text GLabel 4600 4750 0    50   Input ~ 0
1.11
Text GLabel 5400 4350 2    50   Input ~ 0
1.4
Text GLabel 5400 4550 2    50   Input ~ 0
1.8
Text GLabel 5400 4450 2    50   Input ~ 0
1.6
$Comp
L PC104_board-rescue:HEADER_2x26-MLAB_HEADER J1
U 1 1 5D9B2B44
P 5000 5500
F 0 "J1" H 5000 7103 60  0000 C CNN
F 1 "HEADER_2x26" H 5000 6997 60  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x26_P2.54mm_Vertical" H 5000 6891 60  0001 C CNN
F 3 "" H 5000 6750 60  0000 C CNN
	1    5000 5500
	1    0    0    -1  
$EndComp
Text GLabel 5400 4750 2    50   Input ~ 0
1.12
Text GLabel 5400 5650 2    50   Input ~ 0
1.30
Text GLabel 5400 4650 2    50   Input ~ 0
1.10
Wire Wire Line
	6450 5450 6450 5600
Wire Wire Line
	6450 5600 6450 5750
Text GLabel 5400 4250 2    50   Input ~ 0
1.2
Wire Wire Line
	6450 4350 6450 4450
Connection ~ 6450 4250
Wire Wire Line
	6450 4250 6450 4350
Wire Wire Line
	6400 4250 6450 4250
Wire Wire Line
	6400 4350 6450 4350
Wire Wire Line
	6400 4450 6450 4450
Wire Wire Line
	6400 4050 6450 4050
Wire Wire Line
	6400 4150 6450 4150
Wire Wire Line
	6400 4550 6450 4550
Wire Wire Line
	6450 4650 6400 4650
Wire Wire Line
	4600 5450 4750 5450
Wire Wire Line
	4600 5250 4750 5250
Wire Wire Line
	4600 5550 4750 5550
Wire Wire Line
	4600 5350 4750 5350
Wire Wire Line
	5250 4750 5400 4750
Wire Wire Line
	5250 4950 5400 4950
Wire Wire Line
	5250 5150 5400 5150
Wire Wire Line
	5250 5050 5400 5050
Wire Wire Line
	5250 5350 5400 5350
Wire Wire Line
	5250 4850 5400 4850
Wire Wire Line
	5250 5250 5400 5250
Text GLabel 5400 4850 2    50   Input ~ 0
1.14
Text GLabel 4600 4850 0    50   Input ~ 0
1.13
Text GLabel 4600 5150 0    50   Input ~ 0
1.19
Text GLabel 4600 5250 0    50   Input ~ 0
1.21
Text GLabel 4600 4950 0    50   Input ~ 0
1.15
Text GLabel 4600 5050 0    50   Input ~ 0
1.17
Wire Wire Line
	5250 5550 5400 5550
Wire Wire Line
	5250 5450 5400 5450
Wire Wire Line
	5250 5650 5400 5650
Text GLabel 5400 5550 2    50   Input ~ 0
1.28
Text GLabel 5400 5450 2    50   Input ~ 0
1.26
Text GLabel 4600 5550 0    50   Input ~ 0
1.27
Text GLabel 4600 5450 0    50   Input ~ 0
1.25
Text GLabel 4600 5650 0    50   Input ~ 0
1.29
Text GLabel 4600 5350 0    50   Input ~ 0
1.23
Text GLabel 5400 4950 2    50   Input ~ 0
1.16
Text GLabel 5400 5150 2    50   Input ~ 0
1.20
Text GLabel 5400 5050 2    50   Input ~ 0
1.18
Text GLabel 5400 5350 2    50   Input ~ 0
1.24
Text GLabel 5400 5250 2    50   Input ~ 0
1.22
Wire Wire Line
	4600 1800 4750 1800
Wire Wire Line
	4600 2000 4750 2000
Wire Wire Line
	4600 2200 4750 2200
Wire Wire Line
	4600 2400 4750 2400
Text GLabel 4600 2500 0    50   Input ~ 0
1.25
Text GLabel 4600 2600 0    50   Input ~ 0
1.27
Text GLabel 4600 2700 0    50   Input ~ 0
1.29
Text GLabel 5400 2400 2    50   Input ~ 0
1.24
Text GLabel 4600 1700 0    50   Input ~ 0
1.9
Text GLabel 4600 2200 0    50   Input ~ 0
1.19
Text GLabel 4600 2300 0    50   Input ~ 0
1.21
Text GLabel 4600 2400 0    50   Input ~ 0
1.23
Text GLabel 4600 1900 0    50   Input ~ 0
1.13
Text GLabel 4600 2100 0    50   Input ~ 0
1.17
Text GLabel 4600 2000 0    50   Input ~ 0
1.15
Text GLabel 4600 1800 0    50   Input ~ 0
1.11
Wire Wire Line
	5250 2500 5400 2500
Text GLabel 5400 2600 2    50   Input ~ 0
1.28
Text GLabel 5400 2700 2    50   Input ~ 0
1.30
Text GLabel 5400 2500 2    50   Input ~ 0
1.26
$Comp
L PC104_board-rescue:HEADER_2x26-MLAB_HEADER J1
U 1 1 5D9B2A83
P 5000 2550
F 0 "J1" H 5000 4153 60  0000 C CNN
F 1 "HEADER_2x26" H 5000 4047 60  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x26_P2.54mm_Vertical" H 5000 3941 60  0001 C CNN
F 3 "" H 5000 3800 60  0000 C CNN
	1    5000 2550
	1    0    0    -1  
$EndComp
Text GLabel 5400 1700 2    50   Input ~ 0
1.10
Wire Wire Line
	6450 1550 6450 1650
Wire Wire Line
	6400 1650 6450 1650
Wire Wire Line
	6400 2750 6450 2750
Wire Wire Line
	6500 1350 6450 1350
Wire Wire Line
	6450 1350 6450 1450
Wire Wire Line
	6500 1550 6450 1550
Wire Wire Line
	6500 1450 6450 1450
Connection ~ 6450 1450
Wire Wire Line
	6450 1450 6450 1550
Connection ~ 6450 1550
Wire Wire Line
	6400 1350 6450 1350
Connection ~ 6450 1350
Wire Wire Line
	6400 1450 6450 1450
Wire Wire Line
	6400 1550 6450 1550
Wire Wire Line
	6400 1750 6450 1750
Wire Wire Line
	6400 1850 6450 1850
Wire Wire Line
	6400 1950 6450 1950
Wire Wire Line
	6400 2050 6450 2050
Wire Wire Line
	6400 2150 6450 2150
Wire Wire Line
	6400 2250 6450 2250
Wire Wire Line
	6400 2350 6450 2350
Wire Wire Line
	6400 2450 6450 2450
Wire Wire Line
	6400 2550 6450 2550
Wire Wire Line
	6400 2650 6450 2650
Wire Wire Line
	6400 2850 6450 2850
Wire Wire Line
	6400 2950 6450 2950
Wire Wire Line
	6400 3050 6450 3050
Wire Wire Line
	6400 3150 6450 3150
Wire Wire Line
	6400 3250 6450 3250
Wire Wire Line
	6400 3350 6450 3350
Wire Wire Line
	6400 3450 6450 3450
Wire Wire Line
	6400 3550 6450 3550
Wire Wire Line
	6400 3650 6450 3650
Connection ~ 6450 4550
Wire Wire Line
	6450 4550 6450 4650
Connection ~ 6450 4450
Wire Wire Line
	6450 4450 6450 4550
Connection ~ 6450 4350
Connection ~ 6450 4150
Wire Wire Line
	6450 4150 6450 4250
Connection ~ 6450 4050
Wire Wire Line
	6450 4050 6450 4150
Wire Wire Line
	6450 3950 6450 4050
Connection ~ 6450 3950
Connection ~ 6450 3850
Wire Wire Line
	6450 3850 6450 3950
Wire Wire Line
	6400 3750 6450 3750
Wire Wire Line
	6450 3850 6400 3850
Wire Wire Line
	6450 3950 6400 3950
$Comp
L power:GND #PWR0105
U 1 1 5DA445CC
P 6450 5250
F 0 "#PWR0105" H 6450 5000 50  0001 C CNN
F 1 "GND" H 6455 5077 50  0000 C CNN
F 2 "" H 6450 5250 50  0001 C CNN
F 3 "" H 6450 5250 50  0001 C CNN
	1    6450 5250
	-1   0    0    1   
$EndComp
Wire Wire Line
	6450 5250 6450 5300
Wire Wire Line
	6450 5300 6600 5300
Connection ~ 6450 5300
Wire Wire Line
	6450 5300 6450 5450
Wire Wire Line
	6450 5450 6600 5450
Wire Wire Line
	6450 5600 6600 5600
Connection ~ 6450 5450
Wire Wire Line
	6450 5750 6600 5750
Connection ~ 6450 5600
Wire Wire Line
	5250 2100 5400 2100
Wire Wire Line
	5250 2200 5400 2200
Wire Wire Line
	5250 2400 5400 2400
Wire Wire Line
	5250 2300 5400 2300
Wire Wire Line
	4600 2900 4750 2900
Wire Wire Line
	4600 3000 4750 3000
Wire Wire Line
	4600 2700 4750 2700
Text GLabel 4600 2800 0    50   Input ~ 0
1.31
Wire Wire Line
	5250 3000 5400 3000
Wire Wire Line
	5250 2600 5400 2600
Wire Wire Line
	5250 3200 5400 3200
Wire Wire Line
	5250 2700 5400 2700
Wire Wire Line
	5250 2900 5400 2900
Wire Wire Line
	5250 2800 5400 2800
Wire Wire Line
	4600 3200 4750 3200
Wire Wire Line
	4600 3100 4750 3100
Text GLabel 4600 2900 0    50   Input ~ 0
1.33
Text GLabel 4600 3000 0    50   Input ~ 0
1.35
Text GLabel 4600 3100 0    50   Input ~ 0
1.37
Wire Wire Line
	5250 3100 5400 3100
Wire Wire Line
	4600 2600 4750 2600
Wire Wire Line
	4600 2300 4750 2300
Wire Wire Line
	4600 2800 4750 2800
Wire Wire Line
	4600 1900 4750 1900
Wire Wire Line
	5250 1600 5400 1600
Wire Wire Line
	5250 1800 5400 1800
Wire Wire Line
	5250 2000 5400 2000
Wire Wire Line
	4600 2100 4750 2100
Text GLabel 5400 3500 2    50   Input ~ 0
1.46
Text GLabel 5400 3300 2    50   Input ~ 0
1.42
Text GLabel 5400 3400 2    50   Input ~ 0
1.44
Text GLabel 5400 3600 2    50   Input ~ 0
1.48
Text GLabel 5400 3700 2    50   Input ~ 0
1.50
Text GLabel 5400 3000 2    50   Input ~ 0
1.36
Text GLabel 5400 3100 2    50   Input ~ 0
1.38
Text GLabel 5400 2800 2    50   Input ~ 0
1.32
Text GLabel 5400 3200 2    50   Input ~ 0
1.40
Text GLabel 5400 2900 2    50   Input ~ 0
1.34
Text GLabel 5400 3800 2    50   Input ~ 0
1.52
Connection ~ 6450 3750
Wire Wire Line
	6450 3750 6450 3850
Wire Wire Line
	6450 3650 6450 3750
Wire Wire Line
	6500 3650 6450 3650
Connection ~ 6450 3650
Wire Wire Line
	6500 3550 6450 3550
Connection ~ 6450 3550
Wire Wire Line
	6450 3550 6450 3650
$Comp
L power:GND #PWR0101
U 1 1 5DA19578
P 6450 4800
F 0 "#PWR0101" H 6450 4550 50  0001 C CNN
F 1 "GND" H 6455 4627 50  0000 C CNN
F 2 "" H 6450 4800 50  0001 C CNN
F 3 "" H 6450 4800 50  0001 C CNN
	1    6450 4800
	1    0    0    -1  
$EndComp
Wire Wire Line
	6450 4650 6450 4800
Connection ~ 6450 4650
Wire Wire Line
	6500 3450 6450 3450
Connection ~ 6450 3450
Wire Wire Line
	6450 3450 6450 3550
Wire Wire Line
	6450 3350 6450 3450
Wire Wire Line
	6500 3350 6450 3350
Connection ~ 6450 3350
Wire Wire Line
	6500 3250 6450 3250
Connection ~ 6450 3250
Wire Wire Line
	6450 3250 6450 3350
Wire Wire Line
	6450 3150 6450 3250
Connection ~ 6450 3050
Wire Wire Line
	6500 2950 6450 2950
Connection ~ 6450 2950
Wire Wire Line
	6450 2950 6450 3050
Wire Wire Line
	6500 3150 6450 3150
Connection ~ 6450 3150
Wire Wire Line
	6500 3050 6450 3050
Wire Wire Line
	6450 3050 6450 3150
Wire Wire Line
	7600 1200 7600 1350
Wire Wire Line
	6500 2750 6450 2750
$Comp
L power:+5V #PWR0103
U 1 1 5DA33516
P 7600 1200
F 0 "#PWR0103" H 7600 1050 50  0001 C CNN
F 1 "+5V" H 7615 1373 50  0000 C CNN
F 2 "" H 7600 1200 50  0001 C CNN
F 3 "" H 7600 1200 50  0001 C CNN
	1    7600 1200
	1    0    0    -1  
$EndComp
Wire Wire Line
	7550 3250 7600 3250
Wire Wire Line
	7600 3150 7550 3150
Wire Wire Line
	7600 3150 7600 3250
Connection ~ 6450 2750
Wire Wire Line
	6450 2750 6450 2850
Connection ~ 6450 2650
Wire Wire Line
	6450 2650 6450 2750
Wire Wire Line
	6450 2550 6450 2650
Connection ~ 6450 2550
Connection ~ 6450 2450
Wire Wire Line
	6450 2450 6450 2550
Connection ~ 6450 2350
Wire Wire Line
	6450 2350 6450 2450
Wire Wire Line
	7650 2950 7600 2950
Wire Wire Line
	7600 3050 7600 3150
Connection ~ 7600 3050
Wire Wire Line
	7600 3050 7650 3050
Connection ~ 7600 3150
Connection ~ 7600 3250
Wire Wire Line
	7600 3250 7650 3250
$Comp
L power:+5V #PWR0104
U 1 1 5DA42558
P 7500 700
F 0 "#PWR0104" H 7500 550 50  0001 C CNN
F 1 "+5V" H 7515 873 50  0000 C CNN
F 2 "" H 7500 700 50  0001 C CNN
F 3 "" H 7500 700 50  0001 C CNN
	1    7500 700 
	1    0    0    -1  
$EndComp
Wire Wire Line
	7500 700  7500 800 
Wire Wire Line
	7500 900  7650 900 
Wire Wire Line
	7500 800  7650 800 
Connection ~ 7500 800 
Wire Wire Line
	7500 800  7500 900 
Text GLabel 7650 800  2    50   Input ~ 0
2.25
Text GLabel 7650 900  2    50   Input ~ 0
2.26
Wire Wire Line
	6500 2850 6450 2850
Connection ~ 6450 2850
Wire Wire Line
	6450 2850 6450 2950
Wire Wire Line
	7550 2850 7600 2850
Wire Wire Line
	7600 2950 7550 2950
$Comp
L Connector_Generic:Conn_01x32 GND2
U 1 1 5DA113F0
P 6700 2950
F 0 "GND2" H 6780 2942 50  0000 L CNN
F 1 "Conn_01x34" H 6780 2851 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x34_P2.54mm_Vertical" H 6700 2950 50  0001 C CNN
F 3 "~" H 6700 2950 50  0001 C CNN
	1    6700 2950
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x32 GND1
U 1 1 5DA19DBD
P 6200 3050
F 0 "GND1" H 6118 1125 50  0000 C CNN
F 1 "Conn_01x34" H 6118 1216 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x34_P2.54mm_Vertical" H 6200 3050 50  0001 C CNN
F 3 "~" H 6200 3050 50  0001 C CNN
	1    6200 3050
	-1   0    0    1   
$EndComp
Wire Wire Line
	7600 2850 7600 2950
Connection ~ 7600 2850
Wire Wire Line
	7600 2850 7650 2850
Connection ~ 7600 2950
Wire Wire Line
	7550 2750 7600 2750
Wire Wire Line
	7600 2850 7600 2750
Connection ~ 7600 2750
Wire Wire Line
	7600 2750 7650 2750
Wire Wire Line
	7550 2650 7600 2650
Wire Wire Line
	7600 2650 7600 2750
Connection ~ 7600 2650
Wire Wire Line
	7600 2650 7650 2650
Wire Wire Line
	7550 2550 7600 2550
Wire Wire Line
	7600 2550 7600 2650
Connection ~ 7600 2550
Wire Wire Line
	7600 2550 7650 2550
Wire Wire Line
	7600 4650 7600 4550
Connection ~ 7600 4650
Connection ~ 7600 4550
Wire Wire Line
	7600 4550 7550 4550
Wire Wire Line
	7600 4650 7600 4600
Text GLabel 6600 5300 2    50   Input ~ 0
2.29
Text GLabel 6600 5450 2    50   Input ~ 0
2.30
Text GLabel 6600 5600 2    50   Input ~ 0
2.32
Wire Wire Line
	7600 4550 7600 4450
Connection ~ 7600 4450
Wire Wire Line
	7600 4450 7650 4450
Wire Wire Line
	7600 4350 7600 4450
Connection ~ 7600 4350
Wire Wire Line
	7600 4350 7550 4350
Wire Wire Line
	7600 4250 7600 4350
Connection ~ 7600 4250
Wire Wire Line
	7600 4250 7650 4250
Wire Wire Line
	7600 4250 7600 4150
Connection ~ 7600 4150
Wire Wire Line
	7600 4150 7550 4150
Wire Wire Line
	7600 4150 7600 4050
Connection ~ 7600 4050
Wire Wire Line
	7600 4050 7650 4050
Wire Wire Line
	7600 4050 7600 3950
Connection ~ 7600 3950
Wire Wire Line
	7600 3950 7550 3950
Wire Wire Line
	7600 4650 7650 4650
$Comp
L power:+3.3V #PWR0102
U 1 1 5DA32C7A
P 7600 4800
F 0 "#PWR0102" H 7600 4650 50  0001 C CNN
F 1 "+3.3V" H 7615 4973 50  0000 C CNN
F 2 "" H 7600 4800 50  0001 C CNN
F 3 "" H 7600 4800 50  0001 C CNN
	1    7600 4800
	1    0    0    1   
$EndComp
Wire Wire Line
	7600 4800 7600 4650
Wire Wire Line
	7600 5250 7700 5250
$Comp
L power:+3.3V #PWR0106
U 1 1 5DA7230D
P 7600 5200
F 0 "#PWR0106" H 7600 5050 50  0001 C CNN
F 1 "+3.3V" H 7615 5373 50  0000 C CNN
F 2 "" H 7600 5200 50  0001 C CNN
F 3 "" H 7600 5200 50  0001 C CNN
	1    7600 5200
	-1   0    0    -1  
$EndComp
Wire Wire Line
	7600 5200 7600 5250
Wire Wire Line
	7600 5400 7700 5400
Wire Wire Line
	7600 5250 7600 5400
Connection ~ 7600 5250
Text GLabel 7700 5250 2    50   Input ~ 0
2.27
Text GLabel 7700 5400 2    50   Input ~ 0
2.28
Wire Wire Line
	7600 3950 7600 3850
Connection ~ 7600 3850
Wire Wire Line
	7600 3850 7650 3850
Wire Wire Line
	7600 3850 7600 3750
Connection ~ 7600 3750
Wire Wire Line
	7600 3750 7650 3750
Wire Wire Line
	7600 3650 7600 3750
Connection ~ 7600 3650
Wire Wire Line
	7600 3650 7650 3650
Wire Wire Line
	7600 3650 7600 3550
Connection ~ 7600 3550
Wire Wire Line
	7600 3550 7550 3550
Wire Wire Line
	7600 3450 7600 3550
Connection ~ 7600 3450
Wire Wire Line
	7600 3450 7650 3450
Wire Wire Line
	7600 3250 7600 3350
Connection ~ 7600 3350
Wire Wire Line
	7600 3350 7550 3350
Wire Wire Line
	7600 3450 7600 3350
$EndSCHEMATC
