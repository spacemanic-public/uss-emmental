EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text GLabel 1400 3500 0    50   Input ~ 0
1.45
Text GLabel 1400 3400 0    50   Input ~ 0
1.43
Text GLabel 1400 3800 0    50   Input ~ 0
1.51
Text GLabel 1400 3600 0    50   Input ~ 0
1.47
Text GLabel 1400 3700 0    50   Input ~ 0
1.49
Text GLabel 1900 3800 2    50   Input ~ 0
1.52
Text GLabel 1900 3600 2    50   Input ~ 0
1.48
Text GLabel 1900 3500 2    50   Input ~ 0
1.46
Text GLabel 1900 3400 2    50   Input ~ 0
1.44
Text GLabel 2800 3300 0    50   Input ~ 0
1.41
Text GLabel 1900 3300 2    50   Input ~ 0
1.42
Text GLabel 2800 3200 0    50   Input ~ 0
1.39
Text GLabel 1400 3200 0    50   Input ~ 0
1.39
Text GLabel 1400 3300 0    50   Input ~ 0
1.41
Text GLabel 1900 3200 2    50   Input ~ 0
1.40
Text GLabel 2800 3100 0    50   Input ~ 0
1.37
Text GLabel 2800 3000 0    50   Input ~ 0
1.35
Text GLabel 2800 2900 0    50   Input ~ 0
1.33
Text GLabel 2800 2800 0    50   Input ~ 0
1.31
Text GLabel 2800 2700 0    50   Input ~ 0
1.29
Text GLabel 1400 3100 0    50   Input ~ 0
1.37
Text GLabel 1400 2900 0    50   Input ~ 0
1.33
Text GLabel 1400 3000 0    50   Input ~ 0
1.35
Text GLabel 1900 3100 2    50   Input ~ 0
1.38
Text GLabel 1900 2900 2    50   Input ~ 0
1.34
Text GLabel 1900 3000 2    50   Input ~ 0
1.36
Text GLabel 1400 2400 0    50   Input ~ 0
1.23
Text GLabel 1400 2500 0    50   Input ~ 0
1.25
Text GLabel 1400 2300 0    50   Input ~ 0
1.21
Text GLabel 1400 2200 0    50   Input ~ 0
1.19
Text GLabel 1900 2800 2    50   Input ~ 0
1.32
Text GLabel 1400 2600 0    50   Input ~ 0
1.27
Text GLabel 1400 2700 0    50   Input ~ 0
1.29
Text GLabel 1400 2800 0    50   Input ~ 0
1.31
$Comp
L MLAB_HEADER:HEADER_2x26 J1
U 1 1 5D194D38
P 1650 2550
F 0 "J1" H 1650 4153 60  0000 C CNN
F 1 "HEADER_2x26" H 1650 4047 60  0000 C CNN
F 2 "Spacemanic_header:Header_2x26_2.54mm" H 1650 3941 60  0001 C CNN
F 3 "" H 1650 3800 60  0000 C CNN
	1    1650 2550
	1    0    0    -1  
$EndComp
Text GLabel 1400 1900 0    50   Input ~ 0
1.13
Text GLabel 1400 2000 0    50   Input ~ 0
1.15
Text GLabel 1400 2100 0    50   Input ~ 0
1.17
Text GLabel 1400 1800 0    50   Input ~ 0
1.11
Text GLabel 1400 1700 0    50   Input ~ 0
1.9
Text GLabel 1400 1300 0    50   Input ~ 0
1.1
Text GLabel 1400 1600 0    50   Input ~ 0
1.7
Text GLabel 1400 1500 0    50   Input ~ 0
1.5
Text GLabel 1400 1400 0    50   Input ~ 0
1.3
Text GLabel 1900 2000 2    50   Input ~ 0
1.16
Text GLabel 1900 1900 2    50   Input ~ 0
1.14
Text GLabel 1900 1800 2    50   Input ~ 0
1.12
Text GLabel 1900 1700 2    50   Input ~ 0
1.10
Text GLabel 1900 2100 2    50   Input ~ 0
1.18
Text GLabel 1900 2200 2    50   Input ~ 0
1.20
Text GLabel 1900 2600 2    50   Input ~ 0
1.28
Text GLabel 1900 2700 2    50   Input ~ 0
1.30
Text GLabel 1900 2400 2    50   Input ~ 0
1.24
Text GLabel 1900 2300 2    50   Input ~ 0
1.22
Text GLabel 1900 2500 2    50   Input ~ 0
1.26
Text GLabel 1900 1500 2    50   Input ~ 0
1.6
Text GLabel 1900 1300 2    50   Input ~ 0
1.2
Text GLabel 1900 1600 2    50   Input ~ 0
1.8
Text GLabel 1900 1400 2    50   Input ~ 0
1.4
Text GLabel 3300 1400 2    50   Input ~ 0
1.4
Text GLabel 3300 1600 2    50   Input ~ 0
1.8
Text GLabel 3300 1500 2    50   Input ~ 0
1.6
Text GLabel 3300 1700 2    50   Input ~ 0
1.10
Text GLabel 3300 1300 2    50   Input ~ 0
1.2
Text GLabel 2800 1700 0    50   Input ~ 0
1.9
Text GLabel 2800 1400 0    50   Input ~ 0
1.3
Text GLabel 2800 1300 0    50   Input ~ 0
1.1
Text GLabel 2800 1500 0    50   Input ~ 0
1.5
Text GLabel 2800 1600 0    50   Input ~ 0
1.7
Text GLabel 3300 2400 2    50   Input ~ 0
1.24
Text GLabel 3300 2500 2    50   Input ~ 0
1.26
Text GLabel 3300 2300 2    50   Input ~ 0
1.22
Text GLabel 3300 2700 2    50   Input ~ 0
1.30
Text GLabel 3300 2600 2    50   Input ~ 0
1.28
Text GLabel 2800 2600 0    50   Input ~ 0
1.27
Text GLabel 2800 2400 0    50   Input ~ 0
1.23
Text GLabel 2800 2500 0    50   Input ~ 0
1.25
Text GLabel 2800 2200 0    50   Input ~ 0
1.19
Text GLabel 2800 2300 0    50   Input ~ 0
1.21
Text GLabel 3300 2200 2    50   Input ~ 0
1.20
Text GLabel 3300 1900 2    50   Input ~ 0
1.14
Text GLabel 3300 2000 2    50   Input ~ 0
1.16
Text GLabel 3300 2100 2    50   Input ~ 0
1.18
Text GLabel 3300 1800 2    50   Input ~ 0
1.12
Text GLabel 2800 2100 0    50   Input ~ 0
1.17
Text GLabel 2800 2000 0    50   Input ~ 0
1.15
Text GLabel 2800 1800 0    50   Input ~ 0
1.11
Text GLabel 2800 1900 0    50   Input ~ 0
1.13
Text GLabel 3300 2800 2    50   Input ~ 0
1.32
Text GLabel 3300 3100 2    50   Input ~ 0
1.38
Text GLabel 3300 3000 2    50   Input ~ 0
1.36
Text GLabel 3300 2900 2    50   Input ~ 0
1.34
Text GLabel 3300 3300 2    50   Input ~ 0
1.42
Text GLabel 3300 3200 2    50   Input ~ 0
1.40
$Comp
L MLAB_HEADER:HEADER_2x26 J3
U 1 1 5E34AA3A
P 3050 2550
F 0 "J3" H 3050 4153 60  0000 C CNN
F 1 "HEADER_2x26" H 3050 4047 60  0000 C CNN
F 2 "Spacemanic_header:Header_2x26_2.54mm" H 3050 3941 60  0001 C CNN
F 3 "" H 3050 3800 60  0000 C CNN
	1    3050 2550
	1    0    0    -1  
$EndComp
Text GLabel 2800 5950 0    50   Input ~ 0
2.33
Text GLabel 2800 6150 0    50   Input ~ 0
2.37
Text GLabel 2800 6050 0    50   Input ~ 0
2.35
Text GLabel 3300 6150 2    50   Input ~ 0
2.38
Text GLabel 2800 6750 0    50   Input ~ 0
2.49
Text GLabel 2800 6850 0    50   Input ~ 0
2.51
Text GLabel 3300 6850 2    50   Input ~ 0
2.52
Text GLabel 3300 6750 2    50   Input ~ 0
2.50
Text GLabel 3300 6650 2    50   Input ~ 0
2.48
Text GLabel 3300 6550 2    50   Input ~ 0
2.46
Text GLabel 3300 6350 2    50   Input ~ 0
2.42
Text GLabel 3300 6250 2    50   Input ~ 0
2.40
Text GLabel 3300 6450 2    50   Input ~ 0
2.44
Text GLabel 1900 6050 2    50   Input ~ 0
2.36
Text GLabel 1900 5950 2    50   Input ~ 0
2.34
Text GLabel 1900 5850 2    50   Input ~ 0
2.32
Text GLabel 1900 5750 2    50   Input ~ 0
2.30
Text GLabel 1900 5650 2    50   Input ~ 0
2.28
Text GLabel 3300 6050 2    50   Input ~ 0
2.36
Text GLabel 3300 5650 2    50   Input ~ 0
2.28
Text GLabel 3300 5750 2    50   Input ~ 0
2.30
Text GLabel 3300 5950 2    50   Input ~ 0
2.34
Text GLabel 3300 5850 2    50   Input ~ 0
2.32
Text GLabel 2800 6250 0    50   Input ~ 0
2.39
Text GLabel 2800 6650 0    50   Input ~ 0
2.47
Text GLabel 2800 6550 0    50   Input ~ 0
2.45
Text GLabel 2800 6450 0    50   Input ~ 0
2.43
Text GLabel 2800 6350 0    50   Input ~ 0
2.41
Text GLabel 1900 6150 2    50   Input ~ 0
2.38
Text GLabel 1900 6250 2    50   Input ~ 0
2.40
Text GLabel 1900 6350 2    50   Input ~ 0
2.42
Text GLabel 1900 6450 2    50   Input ~ 0
2.44
Text GLabel 1900 6550 2    50   Input ~ 0
2.46
Text GLabel 1900 6650 2    50   Input ~ 0
2.48
Text GLabel 1900 6750 2    50   Input ~ 0
2.50
Text GLabel 1400 6850 0    50   Input ~ 0
2.51
Text GLabel 1900 6850 2    50   Input ~ 0
2.52
Text GLabel 1400 6750 0    50   Input ~ 0
2.49
Text GLabel 1400 6450 0    50   Input ~ 0
2.43
Text GLabel 1400 6650 0    50   Input ~ 0
2.47
Text GLabel 1400 6350 0    50   Input ~ 0
2.41
Text GLabel 1400 6550 0    50   Input ~ 0
2.45
Text GLabel 1400 6250 0    50   Input ~ 0
2.39
Text GLabel 1400 5850 0    50   Input ~ 0
2.31
Text GLabel 1400 5950 0    50   Input ~ 0
2.33
Text GLabel 1400 6150 0    50   Input ~ 0
2.37
Text GLabel 1400 6050 0    50   Input ~ 0
2.35
Text GLabel 1400 5750 0    50   Input ~ 0
2.29
Text GLabel 1400 5650 0    50   Input ~ 0
2.27
Text GLabel 1400 5450 0    50   Input ~ 0
2.23
Text GLabel 1400 5350 0    50   Input ~ 0
2.21
Text GLabel 1400 5550 0    50   Input ~ 0
2.25
$Comp
L MLAB_HEADER:HEADER_2x26 J2
U 1 1 5D197155
P 1650 5600
F 0 "J2" H 1650 7203 60  0000 C CNN
F 1 "HEADER_2x26" H 1650 7097 60  0000 C CNN
F 2 "Spacemanic_header:Header_2x26_2.54mm" H 1650 6991 60  0001 C CNN
F 3 "" H 1650 6850 60  0000 C CNN
	1    1650 5600
	1    0    0    -1  
$EndComp
Text GLabel 1400 5250 0    50   Input ~ 0
2.19
Text GLabel 1400 5150 0    50   Input ~ 0
2.17
Text GLabel 1400 5050 0    50   Input ~ 0
2.15
Text GLabel 1400 4950 0    50   Input ~ 0
2.13
Text GLabel 1400 4850 0    50   Input ~ 0
2.11
Text GLabel 1400 4450 0    50   Input ~ 0
2.3
Text GLabel 1400 4550 0    50   Input ~ 0
2.5
Text GLabel 1400 4350 0    50   Input ~ 0
2.1
Text GLabel 1400 4750 0    50   Input ~ 0
2.9
Text GLabel 1400 4650 0    50   Input ~ 0
2.7
Text GLabel 1900 5250 2    50   Input ~ 0
2.20
Text GLabel 1900 5350 2    50   Input ~ 0
2.22
Text GLabel 1900 5450 2    50   Input ~ 0
2.24
Text GLabel 1900 5550 2    50   Input ~ 0
2.26
Text GLabel 1900 5050 2    50   Input ~ 0
2.16
Text GLabel 1900 5150 2    50   Input ~ 0
2.18
Text GLabel 1900 4750 2    50   Input ~ 0
2.10
Text GLabel 1900 4850 2    50   Input ~ 0
2.12
Text GLabel 1900 4950 2    50   Input ~ 0
2.14
Text GLabel 2800 5750 0    50   Input ~ 0
2.29
Text GLabel 2800 5850 0    50   Input ~ 0
2.31
Text GLabel 2800 5650 0    50   Input ~ 0
2.27
Text GLabel 2800 5450 0    50   Input ~ 0
2.23
Text GLabel 2800 5550 0    50   Input ~ 0
2.25
$Comp
L MLAB_HEADER:HEADER_2x26 J4
U 1 1 5E34ACC1
P 3050 5600
F 0 "J4" H 3050 7203 60  0000 C CNN
F 1 "HEADER_2x26" H 3050 7097 60  0000 C CNN
F 2 "Spacemanic_header:Header_2x26_2.54mm" H 3050 6991 60  0001 C CNN
F 3 "" H 3050 6850 60  0000 C CNN
	1    3050 5600
	1    0    0    -1  
$EndComp
Text GLabel 2800 4650 0    50   Input ~ 0
2.7
Text GLabel 2800 4550 0    50   Input ~ 0
2.5
Text GLabel 2800 4950 0    50   Input ~ 0
2.13
Text GLabel 2800 4850 0    50   Input ~ 0
2.11
Text GLabel 2800 4750 0    50   Input ~ 0
2.9
Text GLabel 2800 5150 0    50   Input ~ 0
2.17
Text GLabel 2800 5250 0    50   Input ~ 0
2.19
Text GLabel 2800 5350 0    50   Input ~ 0
2.21
Text GLabel 2800 5050 0    50   Input ~ 0
2.15
Text GLabel 3300 4950 2    50   Input ~ 0
2.14
Text GLabel 3300 4750 2    50   Input ~ 0
2.10
Text GLabel 3300 4850 2    50   Input ~ 0
2.12
Text GLabel 3300 5050 2    50   Input ~ 0
2.16
Text GLabel 3300 5150 2    50   Input ~ 0
2.18
Text GLabel 3300 5550 2    50   Input ~ 0
2.26
Text GLabel 3300 5350 2    50   Input ~ 0
2.22
Text GLabel 3300 5450 2    50   Input ~ 0
2.24
Text GLabel 3300 5250 2    50   Input ~ 0
2.20
Text GLabel 1900 4650 2    50   Input ~ 0
2.8
Text GLabel 1900 4550 2    50   Input ~ 0
2.6
Text GLabel 1900 4350 2    50   Input ~ 0
2.2
Text GLabel 1900 4450 2    50   Input ~ 0
2.4
Text GLabel 1900 3700 2    50   Input ~ 0
1.50
Text GLabel 2800 4450 0    50   Input ~ 0
2.3
Text GLabel 2800 4350 0    50   Input ~ 0
2.1
Text GLabel 2800 3700 0    50   Input ~ 0
1.49
Text GLabel 2800 3800 0    50   Input ~ 0
1.51
Text GLabel 2800 3600 0    50   Input ~ 0
1.47
Text GLabel 3300 3800 2    50   Input ~ 0
1.52
Text GLabel 3300 3700 2    50   Input ~ 0
1.50
Text GLabel 3300 3600 2    50   Input ~ 0
1.48
Text GLabel 2800 3500 0    50   Input ~ 0
1.45
Text GLabel 2800 3400 0    50   Input ~ 0
1.43
Text GLabel 3300 3500 2    50   Input ~ 0
1.46
Text GLabel 3300 3400 2    50   Input ~ 0
1.44
Text GLabel 3300 4550 2    50   Input ~ 0
2.6
Text GLabel 3300 4650 2    50   Input ~ 0
2.8
Text GLabel 3300 4350 2    50   Input ~ 0
2.2
Text GLabel 3300 4450 2    50   Input ~ 0
2.4
Wire Wire Line
	5900 5650 5900 5550
Connection ~ 5900 5650
Wire Wire Line
	5700 5650 5700 5750
Connection ~ 5700 5650
Wire Wire Line
	5700 5550 5700 5650
$Comp
L power:+5V #PWR0104
U 1 1 5E3B56FD
P 5700 5750
F 0 "#PWR0104" H 5700 5600 50  0001 C CNN
F 1 "+5V" H 5715 5923 50  0000 C CNN
F 2 "" H 5700 5750 50  0001 C CNN
F 3 "" H 5700 5750 50  0001 C CNN
	1    5700 5750
	-1   0    0    1   
$EndComp
$Comp
L power:+5V #PWR0106
U 1 1 5E3C3C80
P 5900 5750
F 0 "#PWR0106" H 5900 5600 50  0001 C CNN
F 1 "+5V" H 5915 5923 50  0000 C CNN
F 2 "" H 5900 5750 50  0001 C CNN
F 3 "" H 5900 5750 50  0001 C CNN
	1    5900 5750
	-1   0    0    1   
$EndComp
Connection ~ 5700 4450
Wire Wire Line
	5700 4350 5700 4450
Wire Wire Line
	5700 4450 5700 4550
Connection ~ 5700 4350
Wire Wire Line
	5700 4250 5700 4350
Wire Wire Line
	5700 4550 5700 4650
Connection ~ 5700 4550
Wire Wire Line
	5700 4650 5700 4750
Connection ~ 5700 4650
Connection ~ 5700 4750
Connection ~ 5700 4850
Wire Wire Line
	5700 4750 5700 4850
Connection ~ 5700 4950
Wire Wire Line
	5700 4850 5700 4950
Wire Wire Line
	5700 4950 5700 5050
Wire Wire Line
	5700 5150 5700 5250
Connection ~ 5700 5250
Connection ~ 5700 5150
Wire Wire Line
	5700 5050 5700 5150
Connection ~ 5700 5050
Wire Wire Line
	5900 5750 5900 5650
Wire Wire Line
	5900 5650 5950 5650
Text GLabel 5950 5650 2    50   Input ~ 0
2.25
Text GLabel 5950 5550 2    50   Input ~ 0
2.26
Wire Wire Line
	5900 5550 5950 5550
Wire Wire Line
	5700 5250 5700 5350
Wire Wire Line
	4650 5150 4650 5250
Wire Wire Line
	4650 5250 4650 5350
Connection ~ 4650 5250
Connection ~ 5700 5450
Wire Wire Line
	5700 5350 5700 5450
Wire Wire Line
	5700 5450 5700 5550
Connection ~ 5700 5350
Connection ~ 5700 5550
Wire Wire Line
	4650 5650 4650 5750
Wire Wire Line
	4650 5550 4650 5650
Wire Wire Line
	4650 5450 4650 5550
Connection ~ 4650 5550
Connection ~ 4650 5450
Connection ~ 4650 5650
Wire Wire Line
	4650 5350 4650 5450
Connection ~ 4650 5350
Wire Wire Line
	4400 5550 4400 5450
Wire Wire Line
	4400 5450 4350 5450
Text GLabel 4350 5450 0    50   Input ~ 0
2.32
Text GLabel 4350 5550 0    50   Input ~ 0
2.30
Wire Wire Line
	4350 5550 4400 5550
Text GLabel 4350 5650 0    50   Input ~ 0
2.29
Wire Wire Line
	4650 4950 4650 5050
Wire Wire Line
	4650 5050 4650 5150
Connection ~ 4650 4950
Connection ~ 4650 5050
Connection ~ 4650 5150
Connection ~ 4400 5550
Wire Wire Line
	4350 5650 4400 5650
Wire Wire Line
	4400 5650 4400 5550
Wire Wire Line
	4400 5750 4400 5650
Connection ~ 4400 5650
$Comp
L power:GND #PWR0101
U 1 1 5E3733AF
P 4650 5750
F 0 "#PWR0101" H 4650 5500 50  0001 C CNN
F 1 "GND" H 4655 5577 50  0000 C CNN
F 2 "" H 4650 5750 50  0001 C CNN
F 3 "" H 4650 5750 50  0001 C CNN
	1    4650 5750
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0102
U 1 1 5E3A1D71
P 4400 5750
F 0 "#PWR0102" H 4400 5500 50  0001 C CNN
F 1 "GND" H 4405 5577 50  0000 C CNN
F 2 "" H 4400 5750 50  0001 C CNN
F 3 "" H 4400 5750 50  0001 C CNN
	1    4400 5750
	1    0    0    -1  
$EndComp
Connection ~ 6800 4150
Connection ~ 6800 3950
Connection ~ 6800 4050
Wire Wire Line
	6800 4050 6800 4150
Wire Wire Line
	6800 3950 6800 4050
Connection ~ 6800 5150
Wire Wire Line
	6800 5150 6800 5250
Wire Wire Line
	6800 5050 6800 5150
Wire Wire Line
	6800 4950 6800 5050
Connection ~ 6800 5050
Connection ~ 6800 4950
Wire Wire Line
	6800 4850 6800 4950
Wire Wire Line
	6800 4750 6800 4850
Connection ~ 6800 4850
Connection ~ 6800 4750
Wire Wire Line
	6800 4650 6800 4750
Connection ~ 6800 4250
Wire Wire Line
	6800 4150 6800 4250
Wire Wire Line
	6800 4250 6800 4350
Wire Wire Line
	6800 4350 6800 4450
Wire Wire Line
	6800 4450 6800 4550
Connection ~ 6800 4450
Connection ~ 6800 4350
Connection ~ 6800 5250
Connection ~ 6800 5350
Connection ~ 6800 5450
Wire Wire Line
	6800 5350 6800 5450
Wire Wire Line
	6800 5250 6800 5350
Wire Wire Line
	6800 4550 6800 4650
Connection ~ 6800 4650
Connection ~ 6800 4550
Wire Wire Line
	7850 5550 7850 5650
Wire Wire Line
	7850 5650 7850 5750
Wire Wire Line
	6800 5450 6800 5550
Wire Wire Line
	6800 5650 6800 5750
Connection ~ 6800 5550
Wire Wire Line
	6800 5550 6800 5650
Connection ~ 6800 5650
$Comp
L power:GND #PWR0105
U 1 1 5E4196F4
P 6800 5750
F 0 "#PWR0105" H 6800 5500 50  0001 C CNN
F 1 "GND" H 6805 5577 50  0000 C CNN
F 2 "" H 6800 5750 50  0001 C CNN
F 3 "" H 6800 5750 50  0001 C CNN
	1    6800 5750
	1    0    0    -1  
$EndComp
Connection ~ 7850 5450
Wire Wire Line
	7850 5350 7850 5450
Connection ~ 7850 5350
Wire Wire Line
	7850 5250 7850 5350
Connection ~ 7850 5250
Connection ~ 7850 5150
Wire Wire Line
	7850 5150 7850 5250
Connection ~ 7850 5050
Wire Wire Line
	7850 5050 7850 5150
Wire Wire Line
	7850 4950 7850 5050
Connection ~ 7850 4950
Wire Wire Line
	7850 4850 7850 4950
Connection ~ 7850 4750
Wire Wire Line
	7850 4650 7850 4750
Connection ~ 7850 4850
Wire Wire Line
	7850 4750 7850 4850
Connection ~ 7850 4550
Connection ~ 7850 4650
Wire Wire Line
	7850 4450 7850 4550
Wire Wire Line
	7850 4550 7850 4650
Connection ~ 7850 4450
Wire Wire Line
	7850 4350 7850 4450
Wire Wire Line
	7850 4250 7850 4350
Connection ~ 7850 4350
Connection ~ 7850 4250
Wire Wire Line
	7850 4150 7850 4250
Wire Wire Line
	7850 3950 7850 4050
Connection ~ 7850 3950
Connection ~ 7850 5550
Connection ~ 7850 5650
Wire Wire Line
	7850 5450 7850 5550
$Comp
L power:+5V #PWR0103
U 1 1 5E4196D3
P 7850 5750
F 0 "#PWR0103" H 7850 5600 50  0001 C CNN
F 1 "+5V" H 7865 5923 50  0000 C CNN
F 2 "" H 7850 5750 50  0001 C CNN
F 3 "" H 7850 5750 50  0001 C CNN
	1    7850 5750
	-1   0    0    1   
$EndComp
Wire Wire Line
	4650 4450 4650 4550
Connection ~ 4650 4650
Connection ~ 4650 4550
Wire Wire Line
	4650 4650 4650 4750
Wire Wire Line
	4650 4550 4650 4650
Wire Wire Line
	4650 4350 4650 4450
Connection ~ 4650 4450
Wire Wire Line
	4650 4250 4650 4350
Connection ~ 4650 4250
Connection ~ 4650 4350
Wire Wire Line
	4650 4150 4650 4250
Wire Wire Line
	4650 4850 4650 4950
Wire Wire Line
	4650 4750 4650 4850
Connection ~ 4650 4750
Connection ~ 4650 4850
Connection ~ 4650 4150
Connection ~ 4650 4050
Connection ~ 4650 3950
Wire Wire Line
	4650 3950 4650 4050
Wire Wire Line
	4650 3850 4650 3950
Wire Wire Line
	4650 3750 4650 3850
Connection ~ 4650 3850
Wire Wire Line
	5700 3750 5700 3850
Connection ~ 5700 3850
Connection ~ 5700 4250
Connection ~ 5700 3950
Wire Wire Line
	5700 3850 5700 3950
Wire Wire Line
	5700 4150 5700 4250
Wire Wire Line
	5700 3950 5700 4050
Wire Wire Line
	4650 4050 4650 4150
Connection ~ 4650 3550
Wire Wire Line
	4650 3650 4650 3750
Wire Wire Line
	4650 3550 4650 3650
Connection ~ 4650 3650
Connection ~ 4650 3750
Connection ~ 5700 2850
Wire Wire Line
	5700 2850 5700 2950
Wire Wire Line
	4650 2850 4650 2950
Wire Wire Line
	4650 2950 4650 3050
Connection ~ 5700 2950
Wire Wire Line
	5700 2950 5700 3050
Connection ~ 5700 3750
Wire Wire Line
	4650 2750 4650 2850
Wire Wire Line
	4650 2650 4650 2750
Connection ~ 4650 2750
Connection ~ 4650 2850
Connection ~ 4650 2650
Connection ~ 4650 2950
Wire Wire Line
	4650 3150 4650 3250
Wire Wire Line
	4650 3050 4650 3150
Connection ~ 4650 3150
Connection ~ 4650 3050
Connection ~ 4650 3250
Wire Wire Line
	4650 3450 4650 3550
Wire Wire Line
	4650 3350 4650 3450
Wire Wire Line
	4650 3250 4650 3350
Connection ~ 4650 3450
Connection ~ 4650 3350
Text GLabel 6100 2550 2    50   Input ~ 0
2.27
Text GLabel 6100 2650 2    50   Input ~ 0
2.28
Connection ~ 6800 2650
Connection ~ 6800 2750
Connection ~ 6800 2850
Wire Wire Line
	6800 2750 6800 2850
Wire Wire Line
	6800 2650 6800 2750
Wire Wire Line
	6800 2550 6800 2650
Wire Wire Line
	6800 3050 6800 3150
Wire Wire Line
	6800 2850 6800 2950
Wire Wire Line
	6800 2950 6800 3050
Connection ~ 6800 3550
Connection ~ 6800 3650
Connection ~ 6800 3450
Connection ~ 6800 3150
Connection ~ 6800 3350
Connection ~ 6800 3250
Wire Wire Line
	6800 3450 6800 3550
Wire Wire Line
	6800 3550 6800 3650
Wire Wire Line
	6800 3350 6800 3450
Wire Wire Line
	6800 3150 6800 3250
Wire Wire Line
	6800 3250 6800 3350
Wire Wire Line
	5700 3450 5700 3550
Wire Wire Line
	5700 3550 5700 3650
Connection ~ 5700 3550
Connection ~ 5700 3650
Wire Wire Line
	5700 3650 5700 3750
Connection ~ 5700 3450
Connection ~ 5700 3150
Wire Wire Line
	5700 3150 5700 3250
Connection ~ 5700 3250
Wire Wire Line
	5700 3350 5700 3450
Wire Wire Line
	5700 3250 5700 3350
Connection ~ 5700 3350
Connection ~ 5700 2650
Connection ~ 5700 2550
Wire Wire Line
	5700 2550 5700 2650
Wire Wire Line
	5700 2450 5700 2550
Wire Wire Line
	6000 2650 6100 2650
Connection ~ 5700 3050
Wire Wire Line
	5700 3050 5700 3150
Wire Wire Line
	5700 2750 5700 2850
Connection ~ 5700 2750
Wire Wire Line
	5700 2650 5700 2750
Wire Wire Line
	6000 2550 6000 2650
Wire Wire Line
	6000 2450 6000 2550
Connection ~ 6000 2550
Wire Wire Line
	6000 2550 6100 2550
Wire Wire Line
	7850 3550 7850 3650
Connection ~ 7850 3650
Connection ~ 7850 3550
Wire Wire Line
	7850 3450 7850 3550
Connection ~ 7850 3450
Connection ~ 7850 3350
Wire Wire Line
	4650 2550 4650 2650
Wire Wire Line
	7850 3250 7850 3350
$Comp
L power:+3V3 #PWR0109
U 1 1 5E3D3E77
P 5700 2450
F 0 "#PWR0109" H 5700 2300 50  0001 C CNN
F 1 "+3V3" H 5715 2623 50  0000 C CNN
F 2 "" H 5700 2450 50  0001 C CNN
F 3 "" H 5700 2450 50  0001 C CNN
	1    5700 2450
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR0108
U 1 1 5E3E51C9
P 6000 2450
F 0 "#PWR0108" H 6000 2300 50  0001 C CNN
F 1 "+3V3" H 6015 2623 50  0000 C CNN
F 2 "" H 6000 2450 50  0001 C CNN
F 3 "" H 6000 2450 50  0001 C CNN
	1    6000 2450
	1    0    0    -1  
$EndComp
Wire Wire Line
	6800 3650 6800 3750
Wire Wire Line
	6800 3750 6800 3850
Wire Wire Line
	6800 3850 6800 3950
Connection ~ 7850 3850
Wire Wire Line
	7850 3850 7850 3950
Wire Wire Line
	7850 3750 7850 3850
Connection ~ 7850 3750
Wire Wire Line
	7850 3650 7850 3750
Wire Wire Line
	7850 2450 7850 2550
Connection ~ 6800 3050
Connection ~ 6800 3750
Connection ~ 6800 3850
Connection ~ 6800 2950
Wire Wire Line
	7850 3350 7850 3450
$Comp
L MLAB_HEADER:HEADER_1x32 J6
U 1 1 5E308496
P 5500 4100
F 0 "J6" H 5475 5775 60  0000 L CNN
F 1 "HEADER_1x32" H 5578 4051 60  0000 L CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x32_P2.54mm_Vertical" H 1600 4800 60  0001 C CNN
F 3 "" H 5500 5650 60  0000 C CNN
	1    5500 4100
	-1   0    0    -1  
$EndComp
$Comp
L MLAB_HEADER:HEADER_1x32 J8
U 1 1 5E41955E
P 7650 4100
F 0 "J8" H 7728 4158 60  0000 L CNN
F 1 "HEADER_1x32" H 7728 4051 60  0000 L CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x32_P2.54mm_Vertical" H 5900 4650 60  0001 C CNN
F 3 "" H 7650 5650 60  0000 C CNN
	1    7650 4100
	-1   0    0    -1  
$EndComp
$Comp
L MLAB_HEADER:HEADER_1x32 J7
U 1 1 5E4193E1
P 7000 4100
F 0 "J7" H 7078 4158 60  0000 L CNN
F 1 "HEADER_1x32" H 7078 4051 60  0000 L CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x32_P2.54mm_Vertical" H 9400 4500 60  0001 C CNN
F 3 "" H 7000 5650 60  0000 C CNN
	1    7000 4100
	1    0    0    -1  
$EndComp
$Comp
L MLAB_HEADER:HEADER_1x32 J5
U 1 1 5E2F6835
P 4850 4100
F 0 "J5" H 4800 5800 60  0000 L CNN
F 1 "HEADER_1x32" H 4928 4105 60  0000 L CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x32_P2.54mm_Vertical" H 7950 4950 60  0001 L CNN
F 3 "" H 4850 5650 60  0000 C CNN
	1    4850 4100
	1    0    0    -1  
$EndComp
Connection ~ 7850 3150
Connection ~ 7850 2550
Wire Wire Line
	7850 3150 7850 3250
Connection ~ 7850 3250
Wire Wire Line
	7850 2550 7850 2650
Connection ~ 7850 2950
Wire Wire Line
	7850 2850 7850 2950
Connection ~ 7850 3050
Wire Wire Line
	7850 2950 7850 3050
Wire Wire Line
	7850 3050 7850 3150
Connection ~ 7850 2650
Wire Wire Line
	7850 2650 7850 2750
Connection ~ 7850 2850
Connection ~ 7850 2750
Wire Wire Line
	7850 2750 7850 2850
$Comp
L power:+3V3 #PWR0107
U 1 1 5E4193B3
P 7850 2450
F 0 "#PWR0107" H 7850 2300 50  0001 C CNN
F 1 "+3V3" H 7865 2623 50  0000 C CNN
F 2 "" H 7850 2450 50  0001 C CNN
F 3 "" H 7850 2450 50  0001 C CNN
	1    7850 2450
	1    0    0    -1  
$EndComp
$Comp
L MLAB_MECHANICAL:HOLE M3
U 1 1 5D096D66
P 9450 2075
F 0 "M3" V 9325 2075 60  0000 C CNN
F 1 "HOLE" V 9350 2075 60  0001 C CNN
F 2 "Mounting_Holes:MountingHole_3.2mm_M3_DIN965_Pad" H 9469 2183 60  0001 C CNN
F 3 "" H 9450 2075 60  0000 C CNN
	1    9450 2075
	0    1    1    0   
$EndComp
$Comp
L MLAB_MECHANICAL:HOLE M4
U 1 1 5D097003
P 9675 2075
F 0 "M4" V 9550 2075 60  0000 C CNN
F 1 "HOLE" V 9575 2100 60  0001 C CNN
F 2 "Mounting_Holes:MountingHole_3.2mm_M3_DIN965_Pad" H 9694 2183 60  0001 C CNN
F 3 "" H 9675 2075 60  0000 C CNN
	1    9675 2075
	0    1    1    0   
$EndComp
$Comp
L MLAB_MECHANICAL:HOLE M2
U 1 1 5D096AD3
P 9200 2075
F 0 "M2" V 9075 2075 60  0000 C CNN
F 1 "HOLE" V 9100 2075 60  0001 C CNN
F 2 "Mounting_Holes:MountingHole_3.2mm_M3_DIN965_Pad" H 9219 2183 60  0001 C CNN
F 3 "" H 9200 2075 60  0000 C CNN
	1    9200 2075
	0    1    1    0   
$EndComp
$Comp
L MLAB_MECHANICAL:HOLE M1
U 1 1 5D096865
P 8950 2075
F 0 "M1" V 8825 2075 60  0000 C CNN
F 1 "HOLE" V 8850 2075 60  0001 C CNN
F 2 "Mounting_Holes:MountingHole_3.2mm_M3_DIN965_Pad" H 8969 2183 60  0001 C CNN
F 3 "" H 8950 2075 60  0000 C CNN
	1    8950 2075
	0    1    1    0   
$EndComp
$Comp
L device:L_Small L1
U 1 1 5E688B75
P 8775 2150
F 0 "L1" V 8868 2150 50  0000 C CNN
F 1 "L_Small" V 8869 2150 50  0001 C CNN
F 2 "Inductors_SMD:L_1210_HandSoldering" H 8775 2150 50  0001 C CNN
F 3 "~" H 8775 2150 50  0001 C CNN
	1    8775 2150
	0    -1   -1   0   
$EndComp
Wire Wire Line
	8875 2150 8950 2150
Wire Wire Line
	8950 2150 8950 2125
$Comp
L power:GND #PWR0110
U 1 1 5E6DACDA
P 8675 2150
F 0 "#PWR0110" H 8675 1900 50  0001 C CNN
F 1 "GND" H 8680 1977 50  0000 C CNN
F 2 "" H 8675 2150 50  0001 C CNN
F 3 "" H 8675 2150 50  0001 C CNN
	1    8675 2150
	0    1    1    0   
$EndComp
$EndSCHEMATC
