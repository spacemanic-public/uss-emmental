# USS Emmental


USS Emmental is a cubesat prototyping board which is open and free to use. Start your space project right away. Download KiCAD project, modify if you want, order PCBs and start to prototyping your space payload.

Spacemanic is a space technology company, primarily focused on nanosatellite technology. We want to make space access easy, fast and cheap.